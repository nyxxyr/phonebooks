import {StyleSheet} from 'react-native'

const card = StyleSheet.create({
    container: {
        margin: 8,
        padding: 8,
        width: '50%',
        backgroundColor: '#fff',
        borderRadius: 4
    },
    text: {
        fontFamily: 'Poppins-Reguler',
        color: '#333',
    }
})

export default card
