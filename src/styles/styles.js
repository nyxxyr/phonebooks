import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    titleBar: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    title: {
        margin: 16,
        fontFamily: 'Poppins-Bold',
        fontSize: 16,
        color: '#333'
    },
    contactList: {
        backgroundColor: '#fff',
        margin: 8,
        borderRadius: 4,
    },
    list: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    form: {
        alignItems: 'center',
    },
    inputForm: {
        margin: 4,
        padding: 8,
        borderWidth: 1,
        borderRadius: 4,
        minWidth: '80%'
    },
    btn: {
        padding: 8,
        borderRadius: 4,
        textAlign: 'center',
        fontFamily: 'Poppins-Regular'
    },
    nodata: {
        alignItems: 'center',
        margin: 32
    }
})

export default styles