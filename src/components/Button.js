import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import styles from '../styles/styles'

const Button = (props) => {
    const color = props.color
    const btn = {
        backgroundColor: color, 
        margin: 8,
        width: '30%',
        borderRadius: 4
    }

    const handleType = (event) => {
        if(event === 'addContact'){
            props.state.handleAddContact()
        }
    }

    return(
        <TouchableOpacity 
            onPress={() => {handleType(props.type)}}
            style={btn}>
            <Text style={styles.btn}>{props.name}</Text>
        </TouchableOpacity>
    )
}

export default Button