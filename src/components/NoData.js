import React from 'react'
import { View, Text } from 'react-native'
import styles from '../styles/styles'

const NoData = () => {
    return(
        <View style={styles.nodata}>
            <Text style={styles.title}>No Contact Found</Text>
        </View>
    )
}

export default NoData
