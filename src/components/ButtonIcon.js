import React from 'react'
import { TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/AntDesign'
import styles from '../styles/styles'

const ButtonIcon = (props) => {
    const handleCondition = () => {
        if(props.type === 'add'){
            props.state.handleShow()
        }
        if(props.type === 'deleteContact'){
            props.state.handleDeleteContact(props.id)
        }
        if(props.type === 'callContact'){
            props.state.handleCall()
        }
    }

    return(
        <TouchableOpacity onPress={handleCondition} style={styles.btn}>
            <Icon name={props.name} size={props.size} color={props.color} />
        </TouchableOpacity>
    )
}

export default ButtonIcon
