import React from 'react'
import { View, Text } from 'react-native'
import card from '../styles/card'

const Card = ({props}) => {
    return(
        <>
            <View key={props.id} style={card.container}>
                <Text style={card.text}>{props.name}</Text>
                <Text style={card.text}>{props.phone}</Text>
            </View>
        </>
    )
}

export default Card
