import React, { useContext } from 'react'
import {
    View,
    Text,
    StatusBar
} from 'react-native'
import { RootContext } from '../api/Context'
import styles from '../styles/styles'
import ButtonIcon from './ButtonIcon'


const Header = () => {
    const state = useContext(RootContext)

    return(
        <>
            <View>
                <StatusBar barStyle="dark-content" backgroundColor="#fff" />
                <View style={styles.titleBar}>
                    <Text style={styles.title}>Phone Book</Text>
                    <View style={{marginRight: 8}}> 
                        <ButtonIcon name="pluscircle" size={30} color="#a8dda8" state={state} type="add" />
                    </View>
                </View>
            </View>
        </>
    )
}

export default Header