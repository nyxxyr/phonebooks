import React, { createContext, useState } from 'react'
import {View, Text} from 'react-native'
import Header from '../components/Header'
import InputForm from '../screens/Home/InputForm'
import Contact from '../screens/Home/Contact'
import NoData from '../components/NoData'

export const RootContext = createContext()

const Context = () => {
    const [show, setShow] = useState(false)
    const [input, setInput] = useState({name: '', phone: ''})
    const [contacts, setContacts] = useState([])

    const handleShow = () => {
        setShow(!show)
    }

    const handleInputName = (e) => {
        setInput({name: e, phone: input.phone})
    }

    const handleInputPhone = (e) => {
        setInput({name: input.name, phone: e})
    }

    const handleAddContact = () => {
        setContacts([...contacts, {id: Math.random(), name: input.name, phone: input.phone}])
        setInput({name: '', phone: ''})
    }

    const handleDeleteContact = (id) => {
        let temp = contacts
        temp = temp.filter(item => {
            return item.id !== id
        })
        setContacts([...temp])
    }

    const handleCall = () => {
        alert("Call is not yet can be used")
    }
    
    return(
        <>
            <RootContext.Provider value={{
                show,
                input,
                contacts,
                handleShow,
                handleInputName,
                handleInputPhone,
                handleAddContact,
                handleDeleteContact,
                handleCall
            }}>
                <Header />
                {
                    show === false ? (<></>) : (<InputForm />)
                }
                {
                    contacts.length > 0 ? (<Contact />) : (<NoData />)
                }
            </RootContext.Provider>
        </>
    )
}

export default Context