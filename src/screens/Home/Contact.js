import React, { useContext } from 'react'
import { ScrollView, View, Text, FlatList } from 'react-native'
import { RootContext } from '../../api/Context'
import ButtonIcon from '../../components/ButtonIcon'
import Card from '../../components/Card'
import styles from '../../styles/styles'

const Item = ({data}) => {
    const props = data.item
    const state = useContext(RootContext)

    return(
        <View style={styles.list}>
            <Card props={props} />
            <View style={{backgroundColor: '#a8dda8', padding: 8}}>
                <ButtonIcon name="phone" size={24} color="#fff" state={state} type="callContact" />
            </View>
            <View style={{backgroundColor: '#af2d2d', padding: 8, marginLeft: 8}}>
                <ButtonIcon name="delete" size={24} color="#fff" state={state} type="deleteContact" id={props.id} />
            </View>
        </View>
    )
}

const Contact = () => {
    const state = useContext(RootContext)
    const showList = (data) => {
        return(
            <Item data={data} />
        )
    }

    return(
        <>
            <ScrollView style={styles.contactList}>
                <FlatList 
                    data={state.contacts}
                    renderItem={showList} />
            </ScrollView>
        </>
    )
}

export default Contact