import React, { useContext } from 'react'
import {
    View,
    TextInput
} from 'react-native'
import { RootContext } from '../../api/Context'
import Button from '../../components/Button'
import styles from '../../styles/styles'

const InputForm = () => {
    const state = useContext(RootContext)

    return(
        <>
            <View style={styles.form}>
                <TextInput
                    value={state.input.name}
                    style={styles.inputForm}
                    onChangeText={state.handleInputName}
                    placeholder="Masukkan Nama" />
                <TextInput
                    value={state.input.phone}
                    style={styles.inputForm}
                    onChangeText={state.handleInputPhone}
                    placeholder="Masukkan Nomor Handphone" />
                <Button name="Tambah" color="#a8dda8" state={state} type="addContact" />
            </View>
        </>
    )
}

export default InputForm